#include<stdio.h>

int faktorial(int x){
	if(x==1)
		return 1;
	else
		return x * faktorial(x-1);
}

int main(){
	int n;
	printf("Masukkan N = ");
	scanf("%d", &n);
	printf("Hasil %d! = %d\n", n, faktorial(n));
	getchar();
	getchar();
	return 0;
}