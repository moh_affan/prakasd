#include<stdio.h>
#include <stdlib.h>

void cetak(char *h, int n)
{
	for (int i = 0; i < n; i++)
		printf("%c", h[i]);
}

void kombinasi(char *h, int l, int r, int n)
{
	int i;
	if (l == r){
		cetak(h, n);
		printf(" ");
	}
	else
	for (i = 0; i < n; i++){
		h[r] = (char)(96 + (i % (n)+1));
		kombinasi(h, l, r - 1, n);
	}
}

void main()
{
	printf("Kombinasi Huruf\n");
	printf("MOH. AFFAN\n");
	printf("NRP.2103167031\n\n");

	int n;
	char lagi = 'y';
	while (lagi == 'y'){
		printf("Masukkan nilai N : "); scanf("%d", &n);
		fflush(stdin);
		char *h = (char*)malloc(sizeof(char)*n);
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
				h[j] = (char)(97 + i);
			kombinasi(h, 0, n - 1, n);
		}
		printf("\n\nLagi ?"); scanf("%c", &lagi);
		fflush(stdin);
	}
}
