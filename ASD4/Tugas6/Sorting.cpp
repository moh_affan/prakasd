#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define N 12

void cetak(int A[N])
{
	int i;
	for (i = 0; i < N; i++)
		printf("%d ", A[i]);
	printf("\n");
}

void cetak_dari(int A[N],int awal, int akhir)
{
	int i;
	for (i = awal; i < akhir; i++)
		printf("%d ", A[i]);
	printf("\n");
}

void insertion(int A[]){
	int i = 1, j, key;
	while (i<N){
		key = A[i];
		j = i - 1;
		while (j >= 0 && A[j]>key)
			A[j + 1] = A[j--];
		A[j + 1] = key;
		cetak(A);
		i++;
	}
}

void selection(int A[]){
	int i = 0, j, min, min_id, temp;
	while (i < N - 1){
		min = A[i];
		min_id = i;
		j = i + 1;
		while (j < N){
			if (A[j] < min){
				min = A[j];
				min_id = j;
			}
			j++;
		}
		/*A[min_id]+=(A[i]-(A[i]=A[min_id]));*/
		temp = A[i];
		A[i] = A[min_id];
		A[min_id] = temp;
		cetak(A);
		i++;
	}
}

void bubble(int A[]){
	int i = 0, j, temp;
	while (i < N - 1){
		j = 0;
		while (j<N - 1 - i){
			if (A[j]>A[j + 1]){
				temp = A[j];
				A[j] = A[j + 1];
				A[j + 1] = temp;
			}
			j++;
		}
		cetak(A);
		i++;
	}
}

void bubbleflag(int A[]){
	int i = 0, j, temp;
	bool did_swap = true;
	while (i < N - 1 && did_swap){
		j = 0;
		did_swap = false;
		while (j<N - 1 - i){
			if (A[j]>A[j + 1]){
				temp = A[j];
				A[j] = A[j + 1];
				A[j + 1] = temp;
				did_swap = true;
			}
			j++;
		}
		i++;
	}
}

void shell(int A[]){
	int i, jarak = N, temp;
	bool did_swap;
	while (jarak >= 1){
		jarak /= 2;
		did_swap = true;
		while (did_swap){
			i = 0;
			did_swap = false;
			while (i<N - jarak){
				if (A[i]>A[i + jarak]){
					temp = A[i];
					A[i] = A[i + jarak];
					A[i + jarak] = temp;
					did_swap = true;
				}
				i++;
			}
		}
		printf("jarak : %d", jarak);
		cetak(A);
	}
}
int partisi(int A[], int p, int r)
{
	int i, j, pivot, temp;
	pivot = A[p];//A[r] atau A[(p+r)/2]
	i = p;
	j = r;
	while (i <= j)
	{
		while (A[i] < pivot)i++;
		while (A[j] > pivot)j--;
		if (i < j)
		{
			temp = A[i];
			A[i] = A[j];
			A[j] = temp;
			i++;
			j--;
		}
		else
			return j;
	}
	cetak_dari(A, p, r);
	return j;
}
void quicksort(int A[], int p, int r)
{
	int q;
	cetak(A);
	if (p < r)
	{
		q = partisi(A, p, r);
		quicksort(A, p, q);
		quicksort(A, q + 1, r);
	}
}

void merge(int A[], int l, int m, int r)
{
	int kiri1, kanan1, kiri2, kanan2, i, j, hasil[N];
	kiri1 = l;
	kanan1 = m;
	kiri2 = m + 1;
	kanan2 = r;
	i = l;
	while (kiri1 <= kanan1 && kiri2 <= kanan2)
	if (A[kiri1] < A[kiri2])
		hasil[i++] = A[kiri1++];
	else
		hasil[i++] = A[kiri2++];
	while (kiri1 <= kanan1)
		hasil[i++] = A[kiri1++];
	while (kiri2 <= kanan2)
		hasil[i++] = A[kiri2++];
	j = l;
	while (j <= r)
		A[j] = hasil[j++];
}

void mergesort(int A[], int l, int r)
{
	int m;
	cetak_dari(A, l, r);
	if (l < r)
	{
		m = (l + r) / 2;
		mergesort(A, l, m);
		mergesort(A, m + 1, r);
		merge(A, l, m, r);
	}
}

void main(){
	int A[N], data[N], i;
	int pilih;
	char lagi = 'y';
	for (i = 0; i < N; i++){
		srand(time(NULL)*(i + 1));
		data[i] = rand() % 100 + 1;
		printf("%d ", data[i]);
	}
	int B[N] = { 34, 8, 56, 21, 67, 33, 14, 5, 65, 28, 82, 12 };
	printf("\n");
	while (lagi == 'y'){
		for (i = 0; i < N; i++)
			A[i] = data[i];
		printf("1. Insertion\n");
		printf("2. Selection\n");
		printf("3. Bubble\n");
		printf("4. Bubule Flag\n");
		printf("5. Shell\n");
		printf("6. Quicksort\n");
		printf("7. Mergesort\n");
		printf("Pilih : ");
		scanf("%d", &pilih);
		//flush input
		fflush(stdin);
		switch (pilih){
		case 1:
			insertion(B);
			break;
		case 2:
			selection(B);
			break;
		case 3:
			bubble(B);
			break;
		case 4:
			bubbleflag(A);
			break;
		case 5:
			shell(B);
			break;
		case 6:
			quicksort(B, 0, N - 1);
			break;
		case 7:
			mergesort(B, 0, N - 1);
			break;
		}
		for (i = 0; i < N; i++)
			printf("%d ", A[i]);
		printf("\n Lagi ? ");
		scanf("%c", &lagi);
	}
}