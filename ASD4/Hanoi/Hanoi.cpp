#include<stdio.h>
int counter = 1;

void hanoi(int disk, char sumber, char tujuan, char perantara)
{
	if (disk > 0)
	{
		hanoi(disk - 1, sumber, perantara, tujuan);
		printf("%d. Pindah disc %d  dari pasak %c ke pasak %c\n", counter++, disk, sumber, tujuan);
		hanoi(disk - 1, perantara, tujuan, sumber);
	}
}

void main()
{
	printf("Menara Hanoi\n");
	printf("MOH. AFFAN\n");
	printf("2103167031\n\n");
	int disk;
	char lagi = 'y';
	while (lagi == 'y')
	{
		printf("Masukkan jumlah disc : ");
		scanf("%d", &disk);
		fflush(stdin);
		printf("Langkah-langkahnya adalah dengan: \n");
		hanoi(disk, 'A', 'C', 'B');
		printf("\nLagi ? ");
		counter = 1;
		scanf("%c", &lagi);
	}
	getchar();
}
