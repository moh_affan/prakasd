#include<stdio.h>
int pascal(int x, int y){
	if (x == y || y == 1)
		return 1;
	else
		return pascal(x - 1, y) + pascal(x - 1, y - 1);
}

void main(){
	printf("Segitiga Pascal Rekursif\n");
	printf("MOH. AFFAN\n");
	printf("NRP.2103167031\n\n");

	int baris;
	char lagi = 'y';
	while (lagi == 'y'){
		printf("Masukkan banyaknya baris : "); scanf("%d", &baris);
		fflush(stdin);
		for (int i = 1; i <= baris; i++){
			for (int k = i; k <= baris; k++)
				printf("  ");
			for (int j = 1; j <= i; j++)
				printf(" %d  ", pascal(i, j));
			printf("\n");
		}
		printf("Lagi ? (y/n) "); scanf("%c", &lagi);
	}
	getchar();
}