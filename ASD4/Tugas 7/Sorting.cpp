#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define N 20

void insertion(int A[], int asc)
{
	int i = 1, j, key;
	while (i < N)
	{
		key = A[i];
		j = i - 1;
		if (asc)
			while (j >= 0 && A[j] > key)
				A[j + 1] = A[j--];
		else
			while (j >= 0 && A[j] < key)
				A[j + 1] = A[j--];
		A[j + 1] = key;
		i++;
	}
}

void selection(int A[], int asc)
{
	int i = 0, j, min, min_id, temp;
	while (i < N - 1)
	{
		min = A[i];
		min_id = i;
		j = i + 1;
		while (j < N)
		{
			if (asc && A[j] < min)
			{
				min = A[j];
				min_id = j;
			}
			else if (!asc && A[j] > min)
			{
				min = A[j];
				min_id = j;
			}
			j++;
		}
		/*A[min_id]+=(A[i]-(A[i]=A[min_id]));*/
		temp = A[i];
		A[i] = A[min_id];
		A[min_id] = temp;
		i++;
	}
}

void bubble(int A[], int asc)
{
	int i = 0, j, temp;
	while (i < N - 1)
	{
		j = 0;
		while (j < N - 1 - i)
		{
			if (asc && A[j] > A[j + 1])
			{
				temp = A[j];
				A[j] = A[j + 1];
				A[j + 1] = temp;
			}
			else if (!asc && A[j] < A[j + 1])
			{
				temp = A[j];
				A[j] = A[j + 1];
				A[j + 1] = temp;
			}
			j++;
		}
		i++;
	}
}

void bubbleflag(int A[], int asc)
{
	int i = 0, j, temp;
	bool did_swap = true;
	while (i < N - 1 && did_swap)
	{
		j = 0;
		did_swap = false;
		while (j < N - 1 - i)
		{
			if (asc && A[j] > A[j + 1])
			{
				temp = A[j];
				A[j] = A[j + 1];
				A[j + 1] = temp;
				did_swap = true;
			}
			else if (!asc && A[j] < A[j + 1])
			{
				temp = A[j];
				A[j] = A[j + 1];
				A[j + 1] = temp;
				did_swap = true;
			}
			j++;
		}
		i++;
	}
}

void shell(int A[], int asc)
{
	int i, jarak = N, temp;
	bool did_swap;
	while (jarak >= 1)
	{
		jarak /= 2;
		did_swap = true;
		while (did_swap)
		{
			i = 0;
			did_swap = false;
			while (i < N - jarak)
			{
				if (asc && A[i] > A[i + jarak])
				{
					temp = A[i];
					A[i] = A[i + jarak];
					A[i + jarak] = temp;
					did_swap = true;
				}
				else if (!asc && A[i] < A[i + jarak])
				{
					temp = A[i];
					A[i] = A[i + jarak];
					A[i + jarak] = temp;
					did_swap = true;
				}
				i++;
			}
		}
	}
}

int partisi(int A[], int p, int r, int asc)
{
	int i, j, pivot, temp;
	pivot = A[p];//A[r] atau A[(p+r)/2]
	i = p;
	j = r;
	while (i <= j)
	{
		if (asc)
		{
			while (A[i] < pivot)i++;
			while (A[j] > pivot)j--;
		}
		else
		{
			while (A[i] > pivot)i++;
			while (A[j] < pivot)j--;
		}
		if (i < j)
		{
			temp = A[i];
			A[i] = A[j];
			A[j] = temp;
			i++;
			j--;
		}
		else
			return j;
	}
	return j;
}

void quicksort(int A[], int p, int r, int asc)
{
	int q;
	if (p < r)
	{
		q = partisi(A, p, r, asc);
		quicksort(A, p, q, asc);
		quicksort(A, q + 1, r, asc);
	}
}

void merge(int A[], int l, int m, int r, int asc)
{
	int kiri1, kanan1, kiri2, kanan2, i, j, hasil[N];
	kiri1 = l;
	kanan1 = m;
	kiri2 = m + 1;
	kanan2 = r;
	i = l;
	while (kiri1 <= kanan1 && kiri2 <= kanan2)
		if (asc && A[kiri1] < A[kiri2])
			hasil[i++] = A[kiri1++];
		else if (!asc && A[kiri1] > A[kiri2])
			hasil[i++] = A[kiri1++];
		else
			hasil[i++] = A[kiri2++];
	while (kiri1 <= kanan1)
		hasil[i++] = A[kiri1++];
	while (kiri2 <= kanan2)
		hasil[i++] = A[kiri2++];
	j = l;
	while (j <= r)
		A[j] = hasil[j++];
}

void mergesort(int A[], int l, int r, int asc)
{
	int m;
	if (l < r)
	{
		m = (l + r) / 2;
		mergesort(A, l, m, asc);
		mergesort(A, m + 1, r, asc);
		merge(A, l, m, r, asc);
	}
}

void main()
{
	printf("Program Sorting\n");
	printf("MOH. AFFAN\n");
	printf("2103167031\n\n");

	int A[N], data[N], i, asc;
	int pilih;
	char lagi = 'y';
	for (i = 0; i < N; i++)
	{
		srand(time(NULL) * (i + 1));
		data[i] = rand() % 100 + 1;
		printf("%d ", data[i]);
	}

	printf("\n");
	while (lagi == 'y')
	{
		for (i = 0; i < N; i++)
			A[i] = data[i];
		printf("1. Insertion\n");
		printf("2. Selection\n");
		printf("3. Bubble\n");
		printf("4. Bubule Flag\n");
		printf("5. Shell\n");
		printf("6. Quicksort\n");
		printf("7. Mergesort\n");
		printf("Pilih : ");
		scanf("%d", &pilih);
		fflush(stdin);
		printf("\nJenis pengurutan : \n1. Ascending\n2. Descending\nPilih : ");
		scanf("%d", &asc);
		asc = asc == 1;
		fflush(stdin);
		switch (pilih)
		{
		case 1:
			insertion(A, asc);
			break;
		case 2:
			selection(A, asc);
			break;
		case 3:
			bubble(A, asc);
			break;
		case 4:
			bubbleflag(A, asc);
			break;
		case 5:
			shell(A, asc);
			break;
		case 6:
			quicksort(A, 0, N - 1, asc);
			break;
		case 7:
			mergesort(A, 0, N - 1, asc);
			break;
		}
		for (i = 0; i < N; i++)
			printf("%d ", A[i]);
		printf("\n Lagi ? ");
		scanf("%c", &lagi);
	}
}
