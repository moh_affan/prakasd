#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define N 14

int binary(int A[], int x)
{
	int l = 0, r = N - 1, m,i;
	bool found = false;
	while (l < r && !found)
	{
		m = (l + r) / 2;
		if (A[m] == x){
			found = true;
			i = m;
		}
		else if (A[l] == x)
		{
			found = true;
			i = l;
		}
		else if (A[r] == x)
		{
			found = true;
			i = r;
		}
		else if (A[m] < x)
			l = m + 1;
		else
			r = m - 1;
	}
	if (found)
		return i;
	return -1;
}

void main()
{
	printf("Binary Search\n");
	printf("MOH. AFFAN\n");
	printf("NRP.2103167031\n\n");

	int A[N] = { 2, 5, 8, 10, 14, 32, 35, 41, 67, 88, 90, 101, 109, 120 }, i, key = 10, index;
	char lagi = 'y';
	printf("Data : ");
	for (i = 0; i < N; i++)
		printf("%d ", A[i]);
	printf("\n");
	while (lagi == 'y')
	{
		printf("Data yang dicari : ");
		scanf("%d", &key);
		fflush(stdin);
		index = binary(A, key);
		printf("Data %d berada pada index ke - %d\n", key, index);
		printf("Lagi ? ");
		scanf("%c", &lagi);
		fflush(stdin);
	}
	getchar();
}
