#include <stdio.h>
#define N 5
#define M 1000

void cetak(int A[N][N], char* jenis)
{
	int i, j;
	printf("%s :\n", jenis);
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			if (A[i][j] < M)
				printf("%d ", A[i][j]);
			else
				printf("M ");
		}
		printf("\n");
	}
}

void warshall(int Q[N][N], int P[N][N], int R[N][N])
{
	int i, j, k;
	for (k = 0; k < N; k++)
		for (i = 0; i < N; i++)
			for (j = 0; j < N; j++)
			{
				if (Q[i][k] + Q[k][j] < Q[i][j])
				{
					Q[i][j] = Q[i][k] + Q[k][j];
					if (R[k][j] == 0)
						R[i][j] = k + 1;
					else
						R[i][j] = R[k][j];
				}
				P[i][j] = P[i][j] | (P[i][k] & P[k][j]);
			}
}


void main()
{
	/*
	 * Matrix Beban
	 * Q = Quantity
	 */
	int Q[N][N] = {M,1,3,M,M,
		M,M,1,M,5,
		3,M,M,2,M,
		M,M,M,M,1,
		M,M,M,M,M};

	/*
	 * Matrix Jalur
	 * P = Path
	 */

	int P[N][N] = {0,1,1,0,0,
		0,0,1,0,1,
		1,0,0,1,0,
		0,0,0,0,1,
		0,0,0,0,0
	};

	/*
	 * Matrix Rute
	 * R = Route
	 */

	int R[N][N] = {M, 0, 0, M, M,
		M, M, 0, M, 0,
		0, M, M, 0, M,
		M, M, M, M, 0,
		M, M, M, M, M};

	printf("Sebelum warshall \n");
	cetak(Q, "Beban");
	cetak(P, "Jalur");
	cetak(R, "Rute");
	warshall(Q, P, R);
	printf("Setelah warshall \n");
	cetak(Q, "Beban");
	cetak(P, "Jalur");
	cetak(R, "Rute");
	getchar();
}
