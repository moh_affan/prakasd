#include <stdio.h>
#define MAX 10
#define N 5
#define M 1000

typedef int Itemtype;

typedef struct
{
	Itemtype item[MAX];
	int front;
	int rear;
	int count;
} Queue;

void Inisialisasi(Queue* q)
{
	q->count = 0;
	q->front = 0;
	q->rear = 0;
}

int Penuh(Queue* q)
{
	return q->count == MAX;
}

int Kosong(Queue* q)
{
	return q->count == 0;
}

void Enqueue(Queue* q, Itemtype x)
{
	if (Penuh(q))printf("Queue penuh\n");
	else
	{
		q->item[q->rear] = x;
		q->rear = ++q->rear % MAX;
		q->count++;
	}
}

Itemtype Dequeue(Queue* q)
{
	Itemtype tmp = -1;
	if (Kosong(q))printf("Queue kosong\n");
	else
	{
		tmp = q->item[q->front];
		q->front = ++q->front % MAX;
		q->count--;
	}
	return tmp;
}

void init_djikstra(int Q[N], int R[N], int asal)
{
	int i;
	for (i = 0; i < N; i++)
	{
		if (i == asal - 1)
			Q[i] = 0;
		else
			Q[i] = M;
		R[i] = 0;
	}
}

int ada_di_queue(Queue* q, int bil)
{
	int ada = 0, i = q->front;
	while (i != q->rear)
	{
		if (q->item[i] == bil)
			ada = 1;
		i = (i + 1) % MAX;
	}
	return ada;
}

void main()
{
	Queue antrian;
	Inisialisasi(&antrian);

	int Q[N], R[N];
	int input[N][N] = {M, 1, 3, M, M,
		M, M, 1, M, 5,
		3, M, M, 2, M,
		M, M, M, M, 1,
		M, M, M, M, M};
	int asal, tujuan, CN;
	printf("Masukkan asal: ");
	scanf("%d", &asal);
	fflush(stdin);
	printf("Masukkan tujuan: ");
	scanf("%d", &tujuan);
	fflush(stdin);
	init_djikstra(Q, R, asal);
	Enqueue(&antrian, asal);
	while (!Kosong(&antrian))
	{
		CN = Dequeue(&antrian);
		for (int i = 0; i < N; i++)
		{
			if (input[CN - 1][i] != M)
			{
				if (Q[CN - 1] + input[CN - 1][i] < Q[i])
				{
					Q[i] = Q[CN - 1] + input[CN - 1][i];
					R[i] = CN;
					if (i != asal - 1 && i != tujuan - 1 && !ada_di_queue(&antrian, i + 1))
						Enqueue(&antrian, i + 1);
				}
			}
		}
	}
	printf("Beban : ");
	for (int i = 0; i < N; i++)
		printf("%d ", Q[i]);
	printf("\nRute : ");
	for (int i = 0; i < N; i++)
		printf("%d ", R[i]);
	getchar();
}
