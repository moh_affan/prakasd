#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define N 50

int squential_unsorted(int A[], int x)
{
	int i = 0;
	bool found = false;
	while (i < N&&!found)
	if (A[i] == x)
		found = true;
	else i++;
	if (found)
		return i;
	return -1;
}

int squential_sorted(int A[], int x)
{
	int i = 0;
	bool found = false;
	while (i < N&&(!found||A[i] > x))
	if (A[i] == x)
		found = true;
	else i++;
	if (found)
		return i;
	return -1;
}

int binary(int A[], int x)
{
	int l = 0, r = N - 1, m;
	bool found = false;
	while (l < r&&!found){
		m = (l + r) / 2;
		if (A[m] == x)found = true;
		else if (A[m] < x)l = m + 1;
		else r = m - 1;
	}
	if (found)return m;
	return -1;
}
int partisi(int A[], int p, int r)
{
	int i, j, pivot, temp;
	pivot = A[p];//A[r] atau A[(p+r)/2]
	i = p;
	j = r;
	while (i <= j)
	{
		while (A[i] < pivot)i++;
		while (A[j] > pivot)j--;
		if (i < j)
		{
			temp = A[i];
			A[i] = A[j];
			A[j] = temp;
			i++;
			j--;
		}
		else
			return j;
	}
	return j;
}
void quicksort(int A[], int p, int r)
{
	int q;
	if (p < r)
	{
		q = partisi(A, p, r);
		quicksort(A, p, q);
		quicksort(A, q + 1, r);
	}
}
void main()
{
	int A[N], data[N], i, key, index;
	int pilih;
	char lagi = 'y';
	for (i = 0; i < N; i++){
		srand(time(NULL)*(i + 1));
		data[i] = rand() % 100 + 1;
		printf("%d ", data[i]);
	}

	printf("\n");
	while (lagi == 'y'){
		for (i = 0; i < N; i++)
			A[i] = data[i];
		printf("\n");
		quicksort(A, 0, N - 1);
		for (i = 0; i < N; i++){
			printf("%d ", A[i]);
		}
		printf("\n");
		key = rand() % 100 + 1;
		printf("Key adalah %d \n", key);
		printf("1. Sequential Unsorted\n");
		printf("2. Sequential Sorted\n");
		printf("3. Binary\n");
		printf("Pilih : ");
		scanf("%d", &pilih);
		//flush input
		fflush(stdin);
		switch (pilih){
		case 1:
			index = squential_unsorted(data, key);
			break;
		case 2:
			index = squential_sorted(A, key);
			break;
		case 3:
			index = binary(A, key);
			break;
		}
		if (index >= 0) printf("Data ditemukan pada index ke-%d\n", index);
		else printf("Data tidak ditemukan\n");
		printf("\nLagi ? (y/n) "); scanf("%c", &lagi);
		fflush(stdin);
	}
}