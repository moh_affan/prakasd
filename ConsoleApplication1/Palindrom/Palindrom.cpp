#include<stdio.h>
#include <string.h>

#define MAX 50

typedef char Itemtype;

typedef struct
{
	Itemtype item[MAX];
	int count;
} Stack;

void Inisialisasi(Stack* s)
{
	s->count = 0;
}

int Kosong(Stack* s)
{
	return s->count == 0;
}

int Penuh(Stack* s)
{
	return s->count == MAX;
}

void Push(Stack* s, Itemtype x)
{
	if (Penuh(s))printf("Stack penuh \n");
	else
	{
		s->item[s->count] = x;
		s->count++;
	}
}

Itemtype Pop(Stack* s)
{
	Itemtype tmp = -1;
	if (Kosong(s))printf("Stack kosong\n");
	else
	{
		s->count--;
		tmp = s->item[s->count];
	}
	return tmp;
}

int Balik(char kata[])
{
	Stack kt;
	Inisialisasi(&kt);
	int c = 0;
	int diff = 0;
	char* tmp = kata;
	for (int i = 0; i < strlen(kata); i++)
		Push(&kt, kata[i]);
	while (!Kosong(&kt))
	{
		char a = kata[c];
		diff += ((tmp[c] = Pop(&kt)) != a);
		c++;
	}
	if (!diff) return 1;
	else return 0;
}


int main()
{
	printf("=======================================================\n");
	printf("===================   MOH AFFAN   =====================\n");
	printf("================ 2 1 0 3 1 6 7 0 3 1 ==================\n");
	printf("===================== PALINDROME ======================\n");
	printf("========================= ASD =========================\n");
	printf("=======================================================\n");
	printf("\n");
	printf("Masukkan Kata : ");
	char kata[MAX];
	scanf("%[^\n]s", &kata);
	printf("Anda memasukkan : %s\n", kata);
	int palindrom = Balik(kata);
	printf("Kata setelah dibalik : %s\n", kata);
	if (palindrom)
		printf("Kata tersebut adalah palindrom\n");
	else
		printf("Kata tersebut adalah bukan palindrom\n");
	getchar();
	getchar();
	return 0;
}
