#include<stdio.h>

typedef struct
{
	float real;
	float im;
} Kompleks;

Kompleks tambah(Kompleks k1, Kompleks k2)
{
	Kompleks tmp;
	tmp.real = k1.real + k2.real;
	tmp.im = k1.im + k2.im;
	return tmp;
}

Kompleks kurang(Kompleks k1, Kompleks k2)
{
	Kompleks tmp;
	tmp.real = k1.real - k2.real;
	tmp.im = k1.im - k2.im;
	return tmp;
}

Kompleks kali(Kompleks k1, Kompleks k2)
{
	Kompleks tmp;
	tmp.real = (k1.real * k2.real) - (k1.im * k2.im);
	tmp.im = (k1.real * k2.im) + (k1.im * k2.real);
	return tmp;
}

Kompleks bagi(Kompleks k1, Kompleks k2)
{
	Kompleks temp;
	temp.real = ((k1.real * k2.real) + (k1.im * k2.im)) / ((k1.real * k1.real) + (k1.im * k1.im));
	temp.im = ((k1.im * k2.real) - (k1.real * k2.im)) / ((k2.real * k2.real) + (k2.im * k2.im));
	return temp;
}

int main()
{
	Kompleks a, b, hasil = {};
	char choice, operasi;

	printf("=======================================================\n");
	printf("===================   MOH AFFAN   =====================\n");
	printf("================ 2 1 0 3 1 6 7 0 3 1 ==================\n");
	printf("=========== PROGRAM OPERASI BILANGAN KOMPLEKS =========\n");
	printf("========================= ASD =========================\n");
	printf("=======================================================\n");
	printf("\n");
	do
	{
		printf("Masukkan bilangan pertama (dipisah dengan spasi): \n");
		scanf("%f %f", &a.real, &a.im);
		printf("Masukkan bilangan kedua (dipisah dengan spasi): \n");
		scanf("%f %f", &b.real, &b.im);
		printf("\nPilihan operasi : \n+ : Penjumlahan\n- : Pengurangan\n* : Perkalian\n/ : Pembagian\nMasukkan pilihan Anda : ");
		scanf("%s", &operasi);
		switch (operasi)
		{
		case '+':
			hasil = tambah(a, b);
			break;
		case '-':
			hasil = kurang(a, b);
			break;
		case '*':
			hasil = kali(a, b);
			break;
		case '/':
			hasil = bagi(a, b);
			break;
		default:
			printf("Operasi yang Anda masukkan salah\n");
			break;
		}
		if (operasi == '+' | operasi == '-' | operasi == '*' | operasi == '/')
			printf("hasil : %5.2f + %5.2fi\n", hasil.real, hasil.im);
		printf("Apakah Anda ingin memasukkan bilangan lain ? (y/t):");
		scanf("%s", &choice);
	} while (choice != 't');
	printf("Terima kasih :)");
	getchar();
	return 0;
}
