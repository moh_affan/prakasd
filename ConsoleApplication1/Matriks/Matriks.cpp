#include<stdio.h>
#define N 2

void tambah(float m1[N][N], float m2[N][N], float h[N][N])
{
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			h[i][j] = m1[i][j] + m2[i][j];
}

void kurang(float m1[N][N], float m2[N][N], float h[N][N])
{
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			h[i][j] = m1[i][j] - m2[i][j];
}

void kali(float m1[N][N], float m2[N][N], float h[N][N])
{
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			h[i][j] = 0;
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			for (int k = 0; k < N; k++)
				h[i][j] += m1[i][k] * m2[k][j];
}

void cetak(float mat[N][N])
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
			printf("%5.2lf ", mat[i][j]);
		printf("\n");
	}
}

int main()
{
	printf("=======================================================\n");
	printf("===================   MOH AFFAN   =====================\n");
	printf("================ 2 1 0 3 1 6 7 0 3 1 ==================\n");
	printf("=============== PROGRAM OPERASI MATRIKS ===============\n");
	printf("========================= ASD =========================\n");
	printf("=======================================================\n");
	printf("\n");

	float a[N][N] = {1, 2,
		3, 4};
	float b[N][N] = {5, 6,
		7, 8};
	float hasil[N][N];

	printf("Matriks A : \n");
	cetak(a);

	printf("Matriks B : \n");
	cetak(b);

	printf("======================================================\n");

	tambah(a, b, hasil);
	printf("Hasil penjumlahan (A+B) : \n");
	cetak(hasil);

	kurang(a, b, hasil);
	printf("Hasil pengurangan (A-B): \n");
	cetak(hasil);

	kali(a, b, hasil);
	printf("Hasil perkalian (A*B): \n");
	cetak(hasil);

	getchar();
	return 0;
}
