#include<stdio.h>
#include<stdlib.h>

typedef struct simpul Node;
struct simpul{
	int data;
	Node *next;
};
Node *head = NULL, *baru;
void allocate_node(int x){
	baru = (Node *)malloc(sizeof(Node));
	if (baru == NULL){
		printf("Alokasi gagal\n"); exit(1);
	}
	else {
		baru->data = x;
		baru->next = NULL;
	}
}
void cetak(){
	Node *p = head;
	while (p != NULL){
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n");
}
void sisip_awal(){
	baru->next = head;
	head = baru;
}
void sisip_akhir(){
	Node *tail = head;
	while (tail->next != NULL)
		tail = tail->next;
	tail->next = baru;
}
void sisip_sebelum(int x){
	Node *before = head;
	if (head->data == x){
		sisip_awal();
	}
	while (before->next != NULL && before->next->data != x){
		before = before->next;
	}
	if (before->next == NULL){
		printf("Not found");
	}
	else {
		baru->next = before->next;
		before->next = baru;
	}
}
void sisip_setelah(int x){
	Node *after = head;
	while (after->data != x){
		after = after->next;
	}
	if (after->next != NULL){
		baru->next = after->next;
		after->next = baru;
	}
	else{
		printf("Not found");
	}
}
void free_node(Node *p){
	free(p);
	p = NULL;
}
void hapus_awal(){
	Node *hapus = head;
	head = hapus->next;
	free_node(hapus);
}

void hapus_akhir(){
	Node *before, *hapus;
	before = hapus = head;
	while (hapus->next != NULL){
		before = hapus;
		hapus = hapus->next;
	}
	before->next = NULL;
	free_node(hapus);
}

void hapus_simpul(int x){
	Node *before, *hapus;
	before = hapus = head;
	if (head->data == x){
		hapus_awal();
	}
	else{
		while (hapus->next != NULL&&hapus->data != x){
			before = hapus;
			hapus = hapus->next;
		}
		if (hapus->next != NULL){
			before->next = hapus->next;
			free_node(hapus);
		}
		else { printf("NOt found"); }
	}
}

int main(){
	allocate_node(10);
	head = baru;
	cetak();
	int dt, pilh, x;
	char lagi = 'y';
	while (lagi == 'y'){
		printf("1. Sisipkan awal \n");
		printf("2. Sisipkan akhir \n");
		printf("3. Sisipkan sebelum simpul \n");
		printf("4. Sisipkan setelah simpul \n");
		printf("5. Hapus awal \n");
		printf("6. Hapus akhir \n");
		printf("7. Hapus tengah \n");
		printf("Pilih : "); fflush(stdin); scanf("%d", &pilh);
		switch (pilh){
		case 1:
			printf("Masukkan data : "); scanf("%d", &dt);
			allocate_node(dt);
			sisip_awal();
			break;
		case 2:
			printf("Masukkan data : "); scanf("%d", &dt);
			allocate_node(dt);
			sisip_akhir();
			break;
		case 3:
			printf("Masukkan data : "); scanf("%d", &dt);
			allocate_node(dt);
			printf("Sisipkan sebelum : "); scanf("%d", &x);
			sisip_sebelum(x);
			break;
		case 4:
			printf("Masukkan data : "); scanf("%d", &dt);
			allocate_node(dt);
			printf("Sisipkan setelah : "); scanf("%d", &x);
			sisip_setelah(x);
			break;
		case 5:
			hapus_awal();
			break;
		case 6:
			hapus_akhir();
			break;
		case 7:
			printf("Masukkan data yang akan dihapus : "); scanf("%d", &x);
			hapus_simpul(x);
			break;
		}
		cetak();
		printf("Lagi ? "); fflush(stdin); scanf("%c", &lagi);
	}
	return 0;
}
