#include<stdio.h>
#include <string.h>
#include <math.h>

#define MAX 10

typedef char Itemtype;

typedef struct
{
	Itemtype item[MAX];
	int count;
} Stack;

void Inisialisasi(Stack* s)
{
	s->count = 0;
}

int Kosong(Stack* s)
{
	return s->count == 0;
}

int Penuh(Stack* s)
{
	return s->count == MAX;
}

void Push(Stack* s, Itemtype x)
{
	if (Penuh(s))printf("Stack penuh \n");
	else
	{
		s->item[s->count] = x;
		s->count++;
	}
}

Itemtype Pop(Stack* s)
{
	Itemtype tmp = -1;
	if (Kosong(s))printf("Stack kosong\n");
	else
	{
		s->count--;
		tmp = s->item[s->count];
	}
	return tmp;
}

Itemtype Top(Stack* s)
{
	Itemtype tmp = -1;
	if (Kosong(s))printf("Stack kosong\n");
	else
	{
		tmp = s->item[s->count - 1];
	}
	return tmp;
}

int IsOperator(char notasi)
{
	if (notasi == '^' | notasi == '*' | notasi == '/')
		return 2;
	else if (notasi == '+' | notasi == '-')
		return 1;
	else
		return 0;
}

void ConvertToPostfix(char infix[])
{
	Stack op;
	Inisialisasi(&op);
	for (int i = 0; i < strlen(infix); i++)
	{
		char k = infix[i];
		int isop = IsOperator(k);
		if (isop == 0)
			if (k == '(')
				Push(&op, k);
			else if (k == ')')
			{
				char x;
				while ((x = Pop(&op)) != '(')
				{
					printf("%c", x);
				}
			}
			else
				printf("%c", k);
		else
		{
			if (Kosong(&op) || isop > IsOperator(Top(&op)))
				Push(&op, k);
			else
			{
				printf("%c", Pop(&op));
				Push(&op, k);
			}
		}
	}
	while (!Kosong(&op))
		printf("%c", Pop(&op));
}

int main()
{
	printf("Konversi Infix ke Postfix\n");
	char operasi[] = "(A+B)*C";
	printf("1. %s => ", operasi);
	ConvertToPostfix(operasi);
	printf("\n");
	char operasi2[] = "(A+B)*(C-D)";
	printf("2. %s => ", operasi2);
	ConvertToPostfix(operasi2);
	printf("\n");
	char operasi3[] = "A*(B+C)/D^E-F*G";
	printf("3. %s => ", operasi3);
	ConvertToPostfix(operasi3);
	getchar();
	return 0;
}
