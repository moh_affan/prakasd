#include<stdio.h>
#include <string.h>
#include <cmath>

#define MAX 20

typedef char Itemtype;

typedef struct
{
	Itemtype item[MAX];
	int count;
} Stack;

void Inisialisasi(Stack* s)
{
	s->count = 0;
}

int Kosong(Stack* s)
{
	return s->count == 0;
}

int Penuh(Stack* s)
{
	return s->count == MAX;
}

void Push(Stack* s, Itemtype x)
{
	if (Penuh(s))printf("Stack penuh \n");
	else
	{
		s->item[s->count] = x;
		s->count++;
	}
}

Itemtype Pop(Stack* s)
{
	Itemtype tmp = -1;
	if (Kosong(s))printf("Stack kosong\n");
	else
	{
		s->count--;
		tmp = s->item[s->count];
	}
	return tmp;
}

Itemtype Top(Stack* s)
{
	Itemtype tmp = -1;
	if (Kosong(s))printf("Stack kosong\n");
	else
	{
		tmp = s->item[s->count - 1];
	}
	return tmp;
}

int IsOperator(char notasi)
{
	if (notasi == '^' | notasi == '*' | notasi == '/')
		return 2;
	else if (notasi == '+' | notasi == '-')
		return 1;
	else
		return 0;
}

void ConvertToPostfix(char infix[])
{
	Stack op;
	Inisialisasi(&op);
	char* tmp = infix;
	int ctr = 0;
	for (int i = 0; i < strlen(infix); i++)
	{
		char k = infix[i];
		int isop = IsOperator(k);
		if (isop == 0)
			if (k == '(')
				Push(&op, k);
			else if (k == ')')
			{
				char x;
				while ((x = Pop(&op)) != '(')
					tmp[ctr++] = x;
			}
			else
				tmp[ctr++] = k;
		else
		{
			if (Kosong(&op) || isop > IsOperator(Top(&op)))
				Push(&op, k);
			else
			{
				tmp[ctr++] = Pop(&op);
				Push(&op, k);
			}
		}
	}
	while (!Kosong(&op))
		tmp[ctr++] = Pop(&op);
	for (int d = ctr; d < strlen(infix); d++)
		tmp[d] = ' ';
}

int OpPostfix(char notasi[])
{
	Stack op;
	Inisialisasi(&op);
	int OpLeft, OpRight;
	for (int i = 0; i < strlen(notasi); i++)
	{
		char k = notasi[i];
		if (k >= '0' && k <= '9')
			Push(&op, k);
		else if (k != ' ')
		{
			OpRight = ((int)Pop(&op)) - 48;
			OpLeft = ((int)Pop(&op)) - 48;
			int hasil = 0;
			switch (k)
			{
			case '+':
				hasil = OpLeft + OpRight;
				break;
			case '-':
				hasil = OpLeft - OpRight;
				break;
			case '*':
				hasil = OpLeft * OpRight;
				break;
			case '/':
				hasil = OpLeft / OpRight;
				break;
			case '^':
				hasil = pow(OpLeft, OpRight);
				break;
			}
			Push(&op, (char)(hasil + 48));
		}
	}
	return ((int)Pop(&op)) - 48;
}

int main()
{
	printf("=======================================================\n");
	printf("===================   MOH AFFAN   =====================\n");
	printf("================ 2 1 0 3 1 6 7 0 3 1 ==================\n");
	printf("================== INFIX TO POSTFIX ===================\n");
	printf("========================= ASD =========================\n");
	printf("=======================================================\n");
	printf("\n");

	printf("MASUKKAN NOTASI INFIX : ");
	char operasi[MAX];
	scanf("%[^\n]s", &operasi);
	printf("Notasi infix yang anda masukkan => %s\n", operasi);
	ConvertToPostfix(operasi);
	printf("Bentuk notasi postfix => %s\n", operasi);
	printf("Hasil operasi postfix => %d", OpPostfix(operasi));
	getchar();
	getchar();
	return 0;
}
