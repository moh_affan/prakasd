#include<stdio.h>
#include <stdlib.h>

typedef int Itemtype;
typedef struct simpul Node;

struct simpul
{
	Itemtype item;
	Node* next;
};

Node* baru;

void allocate_node(int x)
{
	baru = (Node *)malloc(sizeof(Node));
	if (baru == NULL)
	{
		printf("Alokasi gagal\n");
		exit(1);
	}
	else
	{
		baru->item = x;
		baru->next = NULL;
	}
}

void free_node(Node* p)
{
	free(p);
	p = NULL;
}

typedef struct
{
	Node* TOS;
} Stack;

void Inisialisasi(Stack* s)
{
	s->TOS = NULL;
}

int Kosong(Stack* s)
{
	return s->TOS == NULL;
}

void Push(Stack* s, Itemtype x)
{
	allocate_node(x);
	if (s->TOS != NULL)
		baru->next = s->TOS;
	s->TOS = baru;
}

Itemtype Pop(Stack* s)
{
	Itemtype tmp = -1;
	if (Kosong(s))printf("Stack kosong\n");
	else
	{
		tmp = s->TOS->item;
		Node* hapus = s->TOS;
		s->TOS = hapus->next;
		free_node(hapus);
	}
	return tmp;
}

void cetak(Stack* s)
{
	Node* p = s->TOS;
	while (p != NULL)
	{
		printf("%d ", p->item);
		p = p->next;
	}
	printf("\n");
}

int main()
{
	Stack tumpukan;
	Inisialisasi(&tumpukan);
	int pilh;
	Itemtype dt;
	char lagi = 'y';
	while (lagi == 'y')
	{
		printf("1. Push \n");
		printf("2. Pop \n");
		printf("Pilih : ");
		fflush(stdin);
		scanf("%d", &pilh);
		switch (pilh)
		{
		case 1:
			printf("Masukkan data : ");
			scanf("%d", &dt);
			Push(&tumpukan, dt);
			break;
		case 2:
			Itemtype item = Pop(&tumpukan);
			printf("Pop : %d\n",item);
			break;
		}
		cetak(&tumpukan);
		printf("Lagi ? ");
		fflush(stdin);
		scanf("%c", &lagi);
	}
	return 0;
}
