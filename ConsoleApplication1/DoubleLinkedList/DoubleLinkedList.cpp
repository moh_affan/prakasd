#include<stdio.h>
#include<stdlib.h>

typedef struct simpul DNode;

struct simpul
{
	int data;
	DNode* next;
	DNode* prev;
};

DNode *head = NULL, *tail = NULL, *baru;

void allocate_node(int x)
{
	baru = (DNode *)malloc(sizeof(DNode));
	if (baru == NULL)
	{
		printf("Alokasi gagal\n");
		exit(1);
	}
	else
	{
		baru->data = x;
		baru->next = NULL;
		baru->prev = NULL;
	}
}

void cetak_dari_head()
{
	DNode* p = head;
	while (p != NULL)
	{
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n");
}

void cetak_dari_tail()
{
	DNode* p = tail;
	while (p != NULL)
	{
		printf("%d ", p->data);
		p = p->prev;
	}
	printf("\n");
}

void sisip_awal()
{
	baru->next = head;
	head->prev = baru;
	head = baru;
}

void sisip_akhir()
{
	baru->prev = tail;
	tail->next = baru;
	tail = baru;
}

void sisip_setelah(int x)
{
	DNode* after = tail;
	if (tail->data == x)
		sisip_akhir();
	else
	{
		while (after->prev != NULL && after->data != x)
			after = after->prev;
		if (after->prev == NULL && after->data != x)
			printf("Not found\n");
		else
		{
			baru->prev = after;
			baru->next = after->next;
			after->next->prev = baru;
			after->next = baru;
		}
	}
}

void sisip_sebelum(int x)
{
	DNode* before = head;
	if (head->data == x)
		sisip_awal();
	else
	{
		while (before->next != NULL && before->data != x)
			before = before->next;
		if (before->next == NULL && before->data != x)
			printf("Not found\n");
		else
		{
			baru->prev = before->prev;
			baru->next = before;
			before->prev->next = baru;
			before->prev = baru;
		}
	}
}

void free_DNode(DNode* p)
{
	free(p);
	p = NULL;
}

void hapus_awal()
{
	DNode* hapus = head;
	head->next->prev = NULL;
	head = hapus->next;
	free_DNode(hapus);
}

void hapus_akhir()
{
	DNode* hapus = tail;
	tail->prev->next = NULL;
	tail = hapus->prev;
	free_DNode(hapus);
}

void hapus_simpul(int x)
{
	DNode* hapus = head;
	while (hapus->next != NULL && hapus->data != x)
		hapus = hapus->next;
	if (hapus->next == NULL)
		printf("Not found\n");
	else
	{
		hapus->prev->next = hapus->next;
		hapus->next->prev = hapus->prev;
		free_DNode(hapus);
	}
}

int main()
{
	allocate_node(10);
	head = tail = baru;
	printf("Data saat ini : ");
	cetak_dari_head();
	int dt, pilh, x;
	char lagi = 'y';
	while (lagi == 'y')
	{
		printf("1. Sisip Awal \n");
		printf("2. Sisip Akhir \n");
		printf("3. Sisip Sebelum \n");
		printf("4. Sisip Setelah \n");
		printf("5. Hapus Awal\n");
		printf("6. Hapus Akhir\n");
		printf("7. Hapus Simpul\n");
		printf("Pilih : ");
		fflush(stdin);
		scanf("%d", &pilh);
		switch (pilh)
		{
		case 1:
			printf("Masukkan data : ");
			scanf("%d", &dt);
			allocate_node(dt);
			sisip_awal();
			break;
		case 2:
			printf("Masukkan data : ");
			scanf("%d", &dt);
			allocate_node(dt);
			sisip_akhir();
			break;
		case 3:
			printf("Masukkan data : ");
			scanf("%d", &dt);
			printf("Masukkan sebelum simpul : ");
			scanf("%d", &x);
			allocate_node(dt);
			sisip_sebelum(x);
			break;
		case 4:
			printf("Masukkan data : ");
			scanf("%d", &dt);
			printf("Masukkan setelah simpul : ");
			scanf("%d", &x);
			allocate_node(dt);
			sisip_setelah(x);
			break;
		case 5:
			hapus_awal();
			break;
		case 6:
			hapus_akhir();
			break;
		case 7:
			printf("Masukkan data yang akan dihapus : ");
			scanf("%d", &x);
			hapus_simpul(x);
			break;
		}
		cetak_dari_head();
		printf("Lagi ? ");
		fflush(stdin);
		scanf("%c", &lagi);
	}
	return 0;
}
