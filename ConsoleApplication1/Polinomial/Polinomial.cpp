#include<stdio.h>
#include<math.h>
#define M 10

float tambah(float p1[], float p2[], float x){
	float tmp = 0;
	for (int i = 0; i<M; i++)
		tmp += (p1[i] + p2[i])*(float)pow(x, i);
	return tmp;
}

float kurang(float p1[], float p2[], float x){
	float tmp = 0;
	for (int i = 0; i<M; i++)
		tmp += (p1[i] - p2[i])*(float)pow(x, i);
	return tmp;
}

float kali(float p1[], float p2[], float x){
	float tmp = 0;
	for (int i = 0; i<M; i++)
	for (int j = 0; j<M; j++)
		tmp += p1[i] * (float)pow(x, i)*p2[j] * (float)pow(x, j);
	return tmp;
}

float turunan(float p1[], float x){
	float tmp = 0;
	for (int i = 1; i<M; i++)
		tmp += i*p1[i] * (float)pow(x, i - 1);
	return tmp;
}

void cetak(float f[])
{
	for (int j = M - 1; j >= 0; j--)
	{
		if (f[j] != 0)
		{
			if (f[j] != 1)
				printf("%.0f", f[j]);
			if (j != 0)
			{
				printf("x^%d", j);
				printf(" + ");
			}
		}
	}
	printf("\n");
}
void main(){
	float a[M] = { 15, 0, 0, 1, 4, 5, 0, 8, 6, 0 };
	float b[M] = { 10, 0, 2, 2, 3, 0, 0, 4, 0, 3 };
	float c[M] = { 20, 0, 2, 0, 0, 0, 0, 0, 0, 0 };

	printf("=======================================================\n");
	printf("===================   MOH AFFAN   =====================\n");
	printf("================ 2 1 0 3 1 6 7 0 3 1 ==================\n");
	printf("=========== PROGRAM OPERASI BILANGAN POLINOM ==========\n");
	printf("========================= ASD =========================\n");
	printf("=======================================================\n");
	printf("\n");


	printf("P1 = ");
	cetak(a);
	printf("P2 = ");
	cetak(b);
	printf("P3 = ");
	cetak(c);

	printf("\nHasil penambahan P1 dan P2 dengan x=1 : %5.2f\n", tambah(a, b, 1));
	printf("Hasil pengurangan P1 dari P2 dengan x=1 : %5.2f\n", kurang(a, b, 1));
	printf("Hasil perkalian P2 dengan P3 dengan x=1 : %5.2f\n", kali(b, c, 1));
	printf("Hasil turunan P3: %5.2f\n", turunan(c, 1));
	getchar();
}
