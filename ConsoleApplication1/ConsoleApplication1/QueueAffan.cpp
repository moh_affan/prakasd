#include <stdio.h>
#include <math.h>
#define MAX 10

typedef int Itemtype;

typedef struct
{
	Itemtype item[MAX];
	int front;
	int rear;
	int count;
} Queue;

void Inisialisasi(Queue* q)
{
	q->count = 0;
	q->front = 0;
	q->rear = 0;
}

int Penuh(Queue* q)
{
	return q->count == MAX;
}

int Kosong(Queue* q)
{
	return q->count == 0;
}

void Enqueue(Queue* q, Itemtype x)
{
	if (Penuh(q))printf("Queue penuh\n");
	else
	{
		q->item[q->rear] = x;
		q->rear = ++q->rear % MAX;
		q->count++;
	}
}

Itemtype Dequeue(Queue* q)
{
	Itemtype tmp = -1;
	if (Kosong(q))printf("Queue kosong\n");
	else
	{
		tmp = q->item[q->front];
		q->front = ++q->front % MAX;
		q->count--;
	}
	return tmp;
}

Itemtype OpShift(Queue* q, int n, int s)
{
	Itemtype tmp = 0;
	while (n > 0)
	{
		Enqueue(q, n % 2);
		n /= 2;
	}
	for (int i = 0; i < s; i++)
		Enqueue(q, Dequeue(q));
	int e = 0;
	while (!Kosong(q))
	{
		tmp += Dequeue(q) * pow(2.0, e);
		e++;
	}
	return tmp;
}

void main()
{
	Queue queue;
	Inisialisasi(&queue);
	/*Enqueue(&queue, 10);
	Enqueue(&queue, 4);
	printf("Dequeue : %d\n", Dequeue(&queue));*/
	int bil, shift;
	printf("Masukkan bilangan desimal: ");
	scanf("%d", &bil);
	printf("Masukkan shift: ");
	scanf("%d", &shift);
	printf("Hasil %d shift %d adalah = %d", bil, shift, OpShift(&queue, bil, shift));
	getchar();
	getchar();
}
