#include<stdio.h>
#include <string.h>
#include <math.h>

#define MAX 10

typedef int Itemtype;

typedef struct
{
	Itemtype item[MAX];
	int count;
} Stack;

void Inisialisasi(Stack* s)
{
	s->count = 0;
}

int Kosong(Stack* s)
{
	return s->count == 0;
}

int Penuh(Stack* s)
{
	return s->count == MAX;
}

void Push(Stack* s, Itemtype x)
{
	if (Penuh(s))printf("Stack penuh \n");
	else
	{
		s->item[s->count] = x;
		s->count++;
	}
}

Itemtype Pop(Stack* s)
{
	Itemtype tmp = -1;
	if (Kosong(s))printf("Stack kosong\n");
	else
	{
		s->count--;
		tmp = s->item[s->count];
	}
	return tmp;
}

void Konversi(Stack* s, int des, int base)
{
	while (des > 0)
	{
		Push(s, des % base);
		des /= base;
	}
	while (!Kosong(s))
	{
		printf("%x ", Pop(s));
	}
}

void OpPostfix(char notasi[])
{
	Stack OpLeft, OpRight, Postfix;
	for (int i = 0; i < strlen(notasi); i++)
	{
		char k = notasi[i];
		//kode ascii 0 adalah 48
		if (k >= '0' && k <= '9')
		{
			//jika angka maka masukkan ke 
		}
		else
		{
		}
	}
}

int IsOperator(char notasi)
{
	if (notasi == '^' | notasi == '*' | notasi == '/')
		return 1;
	else if (notasi == '+' | notasi == '-')
		return 2;
	else
		return 0;
}

void ConvertToPostfix(char infix[])
{
	Stack op;
	for (int i = 0; i < strlen(infix); i++)
	{
		char k = infix[i];
		int isop = IsOperator(k);
		if (k == '(')
			Push(&op, k);
	}
}


void main()
{
	Stack tumpukan;
	Inisialisasi(&tumpukan);
	int des, base;
	/*Push(&tumpukan, 10);
	Push(&tumpukan, 4);
	printf("Pop : %d\n", Pop(&tumpukan));*/
	printf("Masukkan bilangan desimal: ");
	scanf("%d", &des);
	printf("Masukkan basis (2,8,16): ");
	scanf("%d", &base);
	Konversi(&tumpukan, des, base);
	getchar();
	getchar();
}
