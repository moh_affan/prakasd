#include<stdio.h>
#include<stdlib.h>

typedef struct simpul Node;

struct simpul
{
	int data;
	Node* next;
};

Node *head = NULL, *baru;

void allocate_node(int x)
{
	baru = (Node *)malloc(sizeof(Node));
	if (baru == NULL)
	{
		printf("Alokasi gagal\n");
		exit(1);
	}
	else
	{
		baru->data = x;
		baru->next = NULL;
	}
}

void cetak()
{
	printf("Data saat ini ");
	Node* p = head;
	while (p != NULL)
	{
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n");
}

void sisip_awal()
{
	baru->next = head;
	head = baru;
}

void sisip_akhir()
{
	Node* tail = head;
	while (tail->next != NULL)
		tail = tail->next;
	tail->next = baru;
}

void sisip_sebelum(int x)
{
	Node* before = head;
	while (before->next != NULL && before->next->data != x)
		before = before->next;
	baru->next = before->next;
	before->next = baru;
}

void sisip_setelah(int x)
{
	Node* after = head;
	while (after->next != NULL && after->data != x)
		after = after->next;
	baru->next = after->next;
	after->next = baru;
}

void sisip_simpul()
{
	Node* current = head;
	if (baru->data < head->data)
		sisip_awal();
	else
	{
		while (current->next != NULL)
		{
			if (baru->data > current->data && baru->data < current->next->data)
				sisip_sebelum(current->next->data);
			current = current->next;
		}
		if (current->next == NULL && baru->data > current->data)
			sisip_akhir();
		else
			printf("Data sudah ada\n");
	}
}

void free_node(Node* p)
{
	free(p);
	p = NULL;
}

void hapus_awal()
{
	Node* hapus = head;
	head = hapus->next;
	free_node(hapus);
}

void hapus_akhir()
{
	Node *before, *hapus;
	before = hapus = head;
	while (hapus->next != NULL)
	{
		before = hapus;
		hapus = hapus->next;
	}
	before->next = NULL;
	free_node(hapus);
}

void hapus_tengah(int x)
{
	Node *before, *hapus;
	before = hapus = head;
	while (hapus->data != x)
	{
		before = hapus;
		hapus = hapus->next;
	}
	before->next = hapus->next;
	free_node(hapus);
}

void hapus_simpul(int x)
{
	Node* current = head;
	if (head->data == x)
		hapus_awal();
	else
	{
		while (current->next != NULL)
		{
			if (current->data == x)
			{
				hapus_tengah(current->data);
				break;
			}
			current = current->next;
		}
		if (current->next == NULL)
			if (current->data == x)
				hapus_akhir();
			else
				printf("Data tidak ditemukan\n");
	}
}

int main()
{
	allocate_node(10);
	head = baru;
	cetak();
	int dt, pilh, x;
	char lagi = 'y';
	while (lagi == 'y')
	{
		printf("1. Sisipkan \n");
		printf("2. Hapus\n");
		printf("Pilih : ");
		fflush(stdin);
		scanf("%d", &pilh);
		switch (pilh)
		{
		case 1:
			printf("Masukkan data : ");
			scanf("%d", &dt);
			allocate_node(dt);
			sisip_simpul();
			break;
		case 2:
			printf("Masukkan data yang akan dihapus : ");
			scanf("%d", &x);
			hapus_simpul(x);
			break;
		}
		cetak();
		printf("Lagi ? ");
		fflush(stdin);
		scanf("%c", &lagi);
	}
	return 0;
}
